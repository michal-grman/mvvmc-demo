//
//  AppDelegate.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

import Swinject
import SwinjectAutoregistration

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var assembler:Assembler?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let assembler = Assembler([
            AppAssembly(window),
            UserLoginAssembly(),
            UserAssembly(),
            OrderAssembly(),
            BrowserAssembly()
            ])
        
        self.window = window
        self.assembler = assembler
        
        let router = assembler.resolver ~> UI.Router.self
        
        router.navigation.setRoot("/")
        //router.navigation.open(launchOptions)
        
        //app.start(using: launchOptions ?? "/")
        window.makeKeyAndVisible()
      
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

