
//
//  Enums.swift
//  MVVMC
//
//  Created by Michal Grman on 25/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

extension UI {
    
    public enum ContextFormat {
        
        case root
        case to(deeplink:Deeplink)
    }
    
    public enum PresentableFormat {
        
        case none
        case to(value:PresentableProtocol)
        case toType(of:PresentableProtocol.Type)
        case toStoryboard(name:String, id:String)
    }
    
    public enum AbilityFormat {
        
        case modal
        case push
    }
}


