//
//  DisposableExtension.swift
//  MVVMC
//
//  Created by Michal Grman on 24/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

public protocol DisposableProtocol: class {
    
    var disposing:((DisposableProtocol?) -> ())? { get set }
    var disposable:Disposable { get set }
}

public struct Disposable {
    
    public var add:((DisposableProtocol?) -> ())? {
        set {
            self.base?.disposing = newValue
        }
        get {
            return self.base?.disposing
        }
    }

    public func dispose() {
        self.base?.disposing?(self.base)
    }

    private weak var base:DisposableProtocol?
    
    fileprivate init(_ base:DisposableProtocol?) {
        self.base = base
    }
}

extension DisposableProtocol {
    
    public var disposable:Disposable {
        return Disposable(self)
    }
}
