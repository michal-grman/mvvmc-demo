//
//  PresentableExtension.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

public protocol PresentableProtocol: class {
    
    func toPresent(using options:UI.ContextFormat) -> UIViewController?
}

extension UIViewController: PresentableProtocol {
    
    public func toPresent(using options:UI.ContextFormat = .root) -> UIViewController? {
        return self
    }
}
