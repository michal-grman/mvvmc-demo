//
//  Coordinator.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import Swinject
import SwinjectAutoregistration

public class BaseCoordinator: CoordinatorProtocol, CoordinatorDependencyProtocol {
    
    public var dependencies:[CoordinatorDependencyProtocol] = []
    
    public let resolver:Resolver
    public let navigation:NavigationProtocol
    
    public init(_ resolver:Resolver, navigation:NavigationProtocol) {
        
        self.resolver = resolver
        self.navigation = navigation
    }
    

    public func addResolvedDependency<T:CoordinatorDependencyProtocol>(_ type:T.Type) -> T {
        let dependency = self.resolver ~> type
        defer {
            addDependency(dependency)
        }
        
        return dependency
    }
    
    public func toPresent(using options: UI.ContextFormat = .root) -> UIViewController? {
        return nil;
    }
}
