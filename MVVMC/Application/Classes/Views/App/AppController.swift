//
//  AppController.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

public class AppController: BaseController<AppCoordinator> {

    public static let Identifier = "app-stack"
    
    public var viewModel:AppViewModel?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        //print(self.viewModel)
        // Do any additional setup after loading the view.
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let coordinator = self.coordinator, segue.identifier == AppController.Identifier {
            coordinator.prepare(stack: segue.destination)
        }
    }
}
