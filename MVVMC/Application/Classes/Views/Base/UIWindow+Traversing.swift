//
//  UIWindow+Traversing.swift
//  MVVMC
//
//  Created by Michal Grman on 25/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

extension UIWindow: UITraversingProviderProtocol {}

extension UITraversing where Element:UIWindow {
     
    public func context() -> UIViewController? {
        return self.context(of: self.base.rootViewController)
    }
}
