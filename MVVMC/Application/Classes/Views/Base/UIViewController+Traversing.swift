//
//  UIViewController+Traversing.swift
//  MVVMC
//
//  Created by Michal Grman on 25/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

extension UIViewController: UITraversingProviderProtocol {}

extension UITraversing where Element:UIViewController {
    
    public func context() -> UIViewController? {
        return self.context(of: self.base)
    }
}

