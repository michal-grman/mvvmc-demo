//
//  BaseController.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

public class BaseController<Coordinator:CoordinatorProtocol>: UIViewController, CoordinatedTargetProtocol {

    public weak var coordinator:Coordinator?
}
