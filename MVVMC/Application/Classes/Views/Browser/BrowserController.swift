//
//  BrowserController.swift
//  MVVMC
//
//  Created by Michal Grman on 26/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

public class BrowserController: BaseController<BrowserCoordinator>  {

    public var viewModel:BrowserViewModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
}
