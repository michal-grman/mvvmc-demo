//
//  TaskListController.swift
//  MVVMC
//
//  Created by Michal Grman on 27/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

public class OrderListController: BaseController<OrderCoordinator>  {
    
    public var viewModel:OrderListViewModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "orders"
        
        // Do any additional setup after loading the view.
    }
}

