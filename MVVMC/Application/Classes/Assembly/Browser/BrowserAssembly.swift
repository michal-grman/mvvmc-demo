//
//  BrowserAssembly.swift
//  MVVMC
//
//  Created by Michal Grman on 26/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//


import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

public final class BrowserAssembly: Assembly {
    
    public func assemble(container: Container) {
        
        // Coordinator
        
        container.register(BrowserCoordinator.self) { r in BrowserCoordinator(r, navigation: (r ~> UI.Router.self).navigation) }.inObjectScope(.weak)
        
        // ViewModels
        
        container.register(BrowserViewModel.self) { r in BrowserViewModel(coordinator:r ~> BrowserCoordinator.self) }
        
        // Views
        
        container.storyboardInitCompleted(BrowserController.self) { (r, c) in
            c.coordinator = r ~> BrowserCoordinator.self
            c.viewModel = r ~> BrowserViewModel.self
        }
    }
}
