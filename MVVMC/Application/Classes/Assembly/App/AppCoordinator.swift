//
//  AppCoordinator.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

public final class AppCoordinator: BaseCoordinator {
    
    private var root: UIViewController?
    private var stack: UITabBarController?
    
    public func start() -> PresentableProtocol? {
    
        let storyboard = SwinjectStoryboard.create(name: "App", bundle: Bundle.main, container: self.resolver)
        self.root = storyboard.instantiateInitialViewController()
        
        return self
    }
    
    public override func toPresent(using options: UI.ContextFormat = .root) -> UIViewController? {
        
        return self.root
    }
    
    public func prepare(stack controller:UIViewController) {
     
        let tabs = [
            self.addResolvedDependency(OrderCoordinator.self).start(),
            self.addResolvedDependency(UserCoordinator.self).start()
        ].flatMap{ $0?.toPresent(using:.root) }
        
        self.stack = controller as? UITabBarController
        self.stack?.setViewControllers(tabs, animated:false)
        
    }
}

