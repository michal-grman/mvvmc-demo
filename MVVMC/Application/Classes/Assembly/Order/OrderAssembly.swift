//
//  OrderAssembly.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

public final class OrderAssembly: Assembly {
    
    public func assemble(container: Container) {
        
        // Coordinator
        
        container.register(OrderCoordinator.self) { r in OrderCoordinator(r, navigation: (r ~> UI.Router.self).navigation) }.inObjectScope(.weak)
        
        // ViewModels
        
        container.register(OrderListViewModel.self) { r in OrderListViewModel(coordinator:r ~> OrderCoordinator.self) }
        container.register(OrderDetailViewModel.self) { r in OrderDetailViewModel(coordinator:r ~> OrderCoordinator.self) }
        container.register(OrderAddViewModel.self) { r in OrderAddViewModel(coordinator:r ~> OrderCoordinator.self) }
        
        // Views
        
        container.storyboardInitCompleted(OrderListController.self) { (r, c) in
            c.coordinator = r ~> OrderCoordinator.self
            c.viewModel = r ~> OrderListViewModel.self
        }
        
        container.storyboardInitCompleted(OrderDetailController.self) { (r, c) in
            c.coordinator = r ~> OrderCoordinator.self
            c.viewModel = r ~> OrderDetailViewModel.self
        }
        
        container.storyboardInitCompleted(OrderAddController.self) { (r, c) in
            c.coordinator = r ~> OrderCoordinator.self
            c.viewModel = r ~> OrderAddViewModel.self
        }
    }
}
