//
//  UserCoordinator.swift
//  MVVMC
//
//  Created by Michal Grman on 27/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import UIKit

import Swinject
import SwinjectStoryboard

public final class UserCoordinator: BaseCoordinator {
    
    private var root: UIViewController?
    
    public func start() -> PresentableProtocol? {
        
        let storyboard = SwinjectStoryboard.create(name: "User", bundle: Bundle.main, container: self.resolver)
        self.root = storyboard.instantiateInitialViewController()
        
        return self
    }
    
    public override func toPresent(using options: UI.ContextFormat = .root) -> UIViewController? {
        
        return self.root
    }

}
