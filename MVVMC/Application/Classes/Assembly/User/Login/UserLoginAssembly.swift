//
//  UserAssembly.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

public final class UserLoginAssembly: Assembly {
    
    public func assemble(container: Container) {
    
        // Coordinator
        
        container.register(UserLoginCoordinator.self) { r in UserLoginCoordinator(r, navigation: (r ~> UI.Router.self).navigation) }.inObjectScope(.weak)
        
        // ViewModels
        
        container.register(UserLoginViewModel.self) { r in UserLoginViewModel(coordinator:r ~> UserLoginCoordinator.self) }
        
        // Views
        
        container.storyboardInitCompleted(UserLoginController.self) { (r, c) in
            c.coordinator = r ~> UserLoginCoordinator.self
            c.viewModel = r ~> UserLoginViewModel.self
        }
    }
}

