//
//  UserAssembly.swift
//  MVVMC
//
//  Created by Michal Grman on 27/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//


import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

public final class UserAssembly: Assembly {
    
    
    
    public func assemble(container: Container) {
        
        // Coordinator
        
        container.register(UserCoordinator.self) { r in UserCoordinator(r, navigation: (r ~> UI.Router.self).navigation) }.inObjectScope(.weak)
        
        // ViewModels
        
        container.register(UserListViewModel.self) { r in UserListViewModel(coordinator:r ~> UserCoordinator.self) }
        
        // Views
        
        container.storyboardInitCompleted(UserListController.self) { (r, c) in
            c.coordinator = r ~> UserCoordinator.self
            c.viewModel = r ~> UserListViewModel.self
        }
    }
}
