//
//  BaseViewModel.swift
//  MVVMC
//
//  Created by Michal Grman on 23/06/2017.
//  Copyright © 2017 Thirtyseventy Digital s.r.o. All rights reserved.
//

public class BaseViewModel<Coordinator:CoordinatorProtocol>: CoordinatedTargetProtocol {

    public weak var coordinator: Coordinator?
    
    public required init() {}
}
